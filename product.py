# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL


class Template(ModelSQL, ModelView):
    _name = 'product.template'
    _history = True

Template()


class Product(ModelSQL, ModelView):
    _name = 'product.product'
    _history = True

Product()


class ProductEAV(ModelSQL, ModelView):
    _name = 'product.eav'
    _history = True

ProductEAV()


class ProductAttributes(ModelSQL, ModelView):
    _name = 'product.attribute'
    _history = True

ProductAttributes()


class ProductAttributeOptions(ModelSQL, ModelView):
    _name = 'product.attribute.option'
    _history = True

ProductAttributeOptions()
